import { Schema, model } from 'mongoose';

const translationSchema = new Schema({
	locale: {
		type: String,
		required: true,
		enum: [ 'en', 'nl', 'de' ],
		default: 'en'
	},
	name: {
		type: String,
		required: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_ ]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	createdAt: {
		type: Date,
		required: true,
		default: Date.now
	},
	lastModified: {
		type: Date,
		required: true,
		default: Date.now
	}
});

const rideSchema = new Schema({
	parkId: {
		type: String,
		required: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	originalName: {
		type: String,
		required: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_ ]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	slug: {
		type: String,
		required: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	intensity: {
		type: Number,
		enum: [ 0, 1, 2, 3 ]
	},
	height: {
		type: Number
	},
	length: {
		type: Number
	},
	tempImage: {
		type: String
	},
	rideCategoryIds: {
		type: [ String ],
		required: true
	},
	rideTypeIds: {
		type: [ String ],
		required: true
	},
	translations: [ translationSchema ],
	createdAt: {
		type: Date,
		required: true,
		default: Date.now
	},
	lastModified: {
		type: Date,
		required: true,
		default: Date.now
	}
});

const rideCategoryTranslationSchema = new Schema({
	locale: {
		type: String,
		required: true,
		enum: [ 'en', 'nl', 'de' ],
		default: 'en'
	},
	name: {
		type: String,
		required: false,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_ ]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	createdAt: {
		type: Date,
		required: true,
		default: Date.now
	},
	lastModified: {
		type: Date,
		required: true,
		default: Date.now
	}
});

const rideCategorySchema = new Schema({
	originalName: {
		type: String,
		required: true,
		unique: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_ ]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	slug: {
		type: String,
		required: true,
		unique: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	color: {
		type: String,
		required: true,
		unique: true
	},
	icon: {
		type: String,
		required: true,
		unique: true
	},
	translations: [ rideCategoryTranslationSchema ],
	createdAt: {
		type: Date,
		required: true,
		default: Date.now
	},
	lastModified: {
		type: Date,
		required: true,
		default: Date.now
	}
});

const rideTypeTranslationSchema = new Schema({
	locale: {
		type: String,
		required: true,
		enum: [ 'en', 'nl', 'de' ],
		default: 'en'
	},
	name: {
		type: String,
		required: false,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_ ]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	createdAt: {
		type: Date,
		required: true,
		default: Date.now
	},
	lastModified: {
		type: Date,
		required: true,
		default: Date.now
	}
});

const rideTypeSchema = new Schema({
	originalName: {
		type: String,
		required: true,
		unique: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_ ]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	slug: {
		type: String,
		required: true,
		unique: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	rideCategoryIds: {
		type: [ String ],
		required: true
	},
	translations: [ rideTypeTranslationSchema ],
	createdAt: {
		type: Date,
		required: true,
		default: Date.now
	},
	lastModified: {
		type: Date,
		required: true,
		default: Date.now
	}
});

const Ride = model('Ride', rideSchema);
const RideCategory = model('RideCategory', rideCategorySchema);
const RideType = model('RideType', rideTypeSchema);

export { Ride, RideCategory, RideType };
