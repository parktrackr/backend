import { Schema, model } from 'mongoose';

const translationSchema = new Schema({
	locale: {
		type: String,
		required: true,
		enum: [ 'en', 'nl', 'de' ],
		default: 'en'
	},
	name: {
		type: String,
		required: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_ ]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	createdAt: {
		type: Date,
		required: true,
		default: Date.now
	},
	lastModified: {
		type: Date,
		required: true,
		default: Date.now
	}
});

const conditionSchema = new Schema({
	parkId: {
		type: String,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	rideId: {
		type: String,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	createdAt: {
		type: Date,
		required: true,
		default: Date.now
	},
	lastModified: {
		type: Date,
		required: true,
		default: Date.now
	}
});

const achievementSchema = new Schema({
	color: {
		type: String,
		required: true,
		unique: true
	},
	icon: {
		type: String,
		required: true,
		unique: true
	},
	conditions: [ conditionSchema ],
	translations: [ translationSchema ],
	createdAt: {
		type: Date,
		required: true,
		default: Date.now
	},
	lastModified: {
		type: Date,
		required: true,
		default: Date.now
	}
});

const Achievement = model('Achievement', achievementSchema);

export { Achievement };
