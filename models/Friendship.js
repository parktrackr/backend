import { Schema, model } from 'mongoose';

const friendshipSchema = new Schema({
	requesterId: {
		type: String,
		required: true,
		validate: {
			validator: function(v) {
				return /^[a-zA-Z0-9-_]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	addresseeId: {
		type: String,
		required: true,
		validate: {
			validator: function(v) {
				return /^[a-zA-Z0-9-_@.]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	status: {
		type: String,
		required: true,
		enum: [ 'pending', 'accepted', 'rejected' ],
		default: 'pending'
	},
	createdAt: {
		type: Date,
		required: true,
		default: Date.now
	}
});

const Friendship = model('Friendship', friendshipSchema);

export { Friendship };
