import { Schema, model } from 'mongoose';

const translationSchema = new Schema({
	locale: {
		type: String,
		required: true,
		enum: [ 'en', 'nl', 'de' ],
		default: 'en'
	},
	name: {
		type: String,
		required: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_ ]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	createdAt: {
		type: Date,
		required: true,
		default: Date.now
	},
	lastModified: {
		type: Date,
		required: true,
		default: Date.now
	}
});

const parkSchema = new Schema({
	originalName: {
		type: String,
		required: true,
		unique: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_ ]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	slug: {
		type: String,
		required: true,
		unique: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	countryId: {
		type: String,
		required: false,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	tempImage: {
		type: String
	},
	translations: [ translationSchema ],
	createdAt: {
		type: Date,
		required: true,
		default: Date.now
	},
	lastModified: {
		type: Date,
		required: true,
		default: Date.now
	}
});

const Park = model('Park', parkSchema);

export { Park };
