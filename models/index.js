import { User } from './User';
import { Park } from './Park';
import { Ride, RideCategory, RideType } from './Ride';
import { Friendship } from './Friendship';
import { Achievement } from './Achievement';

export default { User, Park, Ride, RideCategory, RideType, Friendship, Achievement };
