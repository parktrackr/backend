import { Schema, model } from 'mongoose';
import bcrypt from 'bcrypt';

const userParkVisitSchema = new Schema({
	parkId: {
		type: String,
		required: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	visitDate: {
		type: Date,
		required: true,
		default: Date.now
	}
});

const userRideVisitSchema = new Schema({
	rideId: {
		type: String,
		required: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	visitDate: {
		type: Date,
		required: true,
		default: Date.now
	}
});

const userAchievementProgressSchema = new Schema({
	achievementId: {
		type: String,
		required: true,
		validate: {
			validator(v) {
				return /^[a-zA-Z0-9-_]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	completedConditions: {
		type: [ String ]
	},
	isCompleted: {
		type: Boolean,
		required: true
	},
	completionDate: {
		type: Date
	}
});

const userSchema = new Schema({
	username: {
		type: String,
		required: true,
		unique: true,
		validate: {
			validator: function(v) {
				return /^[a-zA-Z0-9-_]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	email: {
		type: String,
		required: true,
		unique: true,
		validate: {
			validator: function(v) {
				return /^[a-zA-Z0-9-_@.]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	password: {
		type: String,
		required: true,
		minLength: 7,
		maxLength: 42,
		validate: {
			validator: function(v) {
				return /^[a-zA-Z0-9-_]+$/.test(v);
			},
			message: '{PATH} must have letters only!'
		}
	},
	role: {
		type: String,
		required: true,
		enum: [ 'user', 'admin' ]
	},
	parkVisits: [ userParkVisitSchema ],
	rideVisits: [ userRideVisitSchema ],
	achievementProgress: [ userAchievementProgressSchema ],
	registerDate: {
		type: Date,
		required: true,
		default: Date.now
	}
});

userSchema.statics.findByLogin = async function(login) {
	return User.findOne({ username: login }, function(err, userObj) {
		if (err) {
			console.log(err);
		} else if (userObj) {
			return userObj;
		} else {
			return null;
		}
	});
};

userSchema.pre('save', async function(next) {
	this.password = await User.generatePasswordHash(this.password);
	next();
});

userSchema.statics.generatePasswordHash = async function(password) {
	const saltRounds = 10;
	return await bcrypt.hash(password, saltRounds);
};

userSchema.methods.validatePassword = async function(password) {
	return await bcrypt.compare(password, this.password);
};

const User = model('User', userSchema);

export { User };
