import { GraphQLDateTime } from 'graphql-iso-date';

import userResolvers from './user';
import parkResolvers from './park';
import rideResolvers from './ride';
import friendshipResolvers from './friendship';
import achievementResolvers from './achievement';

const customScalarResolver = {
	Date: GraphQLDateTime
};

export default [
	customScalarResolver,
	userResolvers,
	friendshipResolvers,
	parkResolvers,
	rideResolvers,
	achievementResolvers
];
