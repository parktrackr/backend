import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated } from './authorization';

export default {
	Query: {
		rides: (parent, { offset = 0, limit = 100, filter = null }, { Ride }) => {
			if (filter === null) {
				return Ride.find({}).skip(offset).limit(limit);
			} else {
				return Ride.find({ originalName: { $regex: filter, $options: 'i' } });
			}
		},
		ride: async (parent, { id, slug }, { Ride }) => {
			if (id) {
				return Ride.findById(id);
			}
			if (slug) {
				const ride = await Ride.findOne({ slug });
				return ride;
			}
		},
		rideCategories: (parent, { offset = 0, limit = 100 }, { RideCategory }) => {
			return RideCategory.find({}).skip(offset).limit(limit);
		},
		rideCategory: async (parent, { id, slug }, { RideCategory }) => {
			if (id) {
				return RideCategory.findById(id);
			}
			if (slug) {
				const rideCategory = await RideCategory.findOne({ slug });
				return rideCategory;
			}
		},
		rideTypes: (parent, { offset = 0, limit = 100 }, { RideType }) => {
			return RideType.find({}).skip(offset).limit(limit);
		},
		rideType: async (parent, { id, slug }, { RideType }) => {
			if (id) {
				return RideType.findById(id);
			}
			if (slug) {
				const rideType = await RideType.findOne({ slug });
				return rideType;
			}
		}
	},
	Mutation: {
		createRide: combineResolvers(
			isAuthenticated,
			async (
				parent,
				{ parkId, name, rideCategoryIds, rideTypeIds, tempImage, height, length, intensity },
				{ Ride }
			) => {
				const ride = new Ride({
					parkId,
					originalName: name,
					slug: name.toLowerCase().replace(/ /g, '-'),
					rideCategoryIds,
					rideTypeIds,
					intensity,
					height,
					length,
					tempImage,
					translations: [
						{
							name
						}
					]
				});

				return ride.save();
			}
		),
		deleteRide: combineResolvers(async (parent, { id }, { Ride }) => {
			return await Ride.findOneAndDelete({ _id: id }, { maxTimeMS: 5 })
				.then((resp) => {
					if (resp) {
						return true;
					} else {
						return false;
					}
				})
				.catch(() => {
					return false;
				});
		}),
		createRideCategory: combineResolvers(
			isAuthenticated,
			async (parent, { name, color, icon }, { RideCategory }) => {
				const rideCategory = new RideCategory({
					originalName: name,
					slug: name.toLowerCase().replace(/ /g, '-'),
					color,
					icon,
					translations: [
						{
							name
						}
					]
				});

				return rideCategory.save();
			}
		),
		deleteRideCategory: combineResolvers(async (parent, { id }, { RideCategory }) => {
			return await RideCategory.findOneAndDelete({ _id: id }, { maxTimeMS: 5 })
				.then((resp) => {
					if (resp) {
						return true;
					} else {
						return false;
					}
				})
				.catch(() => {
					return false;
				});
		}),
		updateRideCategory: async (parent, { id, name, color, icon }, { RideCategory }) => {
			const existingRideCategory = await RideCategory.findById(id);
			if (existingRideCategory === null) {
				return null;
			}

			const existingTranslation = existingRideCategory.translations.find((trans) => trans.locale === 'en');

			return RideCategory.findOneAndUpdate(
				{ _id: id, 'translations._id': existingTranslation.id },
				{
					$set: {
						originalName: name,
						color,
						icon,
						slug: name.toLowerCase().replace(/ /g, '-'),
						'translations.$.name': name
					}
				}
			);
		},
		createRideType: combineResolvers(isAuthenticated, async (parent, { name, rideCategoryIds }, { RideType }) => {
			const rideType = new RideType({
				originalName: name,
				slug: name.toLowerCase().replace(/ /g, '-'),
				rideCategoryIds,
				translations: [
					{
						name
					}
				]
			});

			return rideType.save();
		}),
		deleteRideType: combineResolvers(async (parent, { id }, { RideType }) => {
			return await RideType.findOneAndDelete({ _id: id }, { maxTimeMS: 5 })
				.then((resp) => {
					if (resp) {
						return true;
					} else {
						return false;
					}
				})
				.catch(() => {
					return false;
				});
		}),
		updateRide: async (
			parent,
			{ id, name, parkId, rideCategoryIds, rideTypeIds, intensity, height, length, tempImage },
			{ Ride }
		) => {
			const existingRide = await Ride.findById(id);
			if (existingRide === null) {
				return null;
			}

			const existingTranslation = existingRide.translations.find((trans) => trans.locale === 'en');

			return await Ride.findOneAndUpdate(
				{ _id: id, 'translations._id': existingTranslation.id },
				{
					$set: {
						originalName: name,
						parkId,
						slug: name.toLowerCase().replace(/ /g, '-'),
						rideCategoryIds,
						rideTypeIds,
						intensity,
						height,
						length,
						tempImage: tempImage,
						'translations.$.name': name
					}
				}
			);
		}
	},
	Ride: {
		translation: (parent, { locale }) => {
			return parent.translations.find((doc) => doc.locale === locale);
		},
		park: (parent, args, { Park }) => {
			return Park.findById(parent.parkId);
		},
		rideCategories: (parent, args, { RideCategory }) => {
			if (parent.rideCategoryIds.length > 0) {
				return RideCategory.find().where('_id').in(parent.rideCategoryIds).exec();
			}
			return null;
		},
		rideTypes: (parent, args, { RideType }) => {
			if (parent.rideTypeIds.length > 0) {
				return RideType.find().where('_id').in(parent.rideTypeIds).exec();
			}
			return null;
		}
	},
	RideCategory: {
		translation: (parent, { locale }) => {
			return parent.translations.find((doc) => doc.locale === locale);
		},
		rideTypes: (parent, args, { RideType }) => {
			return RideType.find({ rideCategoryIds: { $in: parent.id } });
		}
	},
	RideType: {
		translation: (parent, { locale }) => {
			return parent.translations.find((doc) => doc.locale === locale);
		},
		rideCategories: (parent, args, { RideCategory }) => {
			if (parent.rideCategoryIds.length > 0) {
				return RideCategory.find().where('_id').in(parent.rideCategoryIds).exec();
			}
			return null;
		}
	}
};
