const renderConditions = (array, type) => {
	return array.map((obj) => {
		return {
			[type]: obj
		};
	});
};

export default {
	Query: {
		achievements: (parent, args, { Achievement }) => {
			return Achievement.find({});
		},
		achievement: async (parent, { id }, { Achievement }) => {
			return Achievement.findById(id);
		}
	},
	Mutation: {
		createAchievement: async (parent, { name, color, icon, rideIds, parkIds }, { Achievement }) => {
			let conditions = [];
			if (rideIds) {
				conditions = renderConditions(rideIds, 'rideId');
			}
			if (parkIds) {
				conditions = renderConditions(parkIds, 'parkId');
			}
			const achievement = new Achievement({
				color,
				icon,
				conditions,
				translations: [
					{
						name
					}
				]
			});

			return achievement.save();
		},
		deleteAchievement: async (parent, { id }, { Achievement }) => {
			return await Achievement.findOneAndDelete({ _id: id }, { maxTimeMS: 5 })
				.then((resp) => {
					if (resp) {
						return true;
					} else {
						return false;
					}
				})
				.catch(() => {
					return false;
				});
		},
		updateAchievement: async (parent, { id, name, color, icon, rideIds }, { Achievement }) => {
			const existingAchievement = await Achievement.findById(id);
			if (existingAchievement === null) {
				return null;
			}

			const existingTranslation = existingAchievement.translations.find((trans) => trans.locale === 'en');

			let conditions = [];
			if (rideIds) {
				conditions = renderConditions(rideIds, 'rideId');
			}
			return await Achievement.findOneAndUpdate(
				{ _id: id, 'translations._id': existingTranslation.id },
				{
					$set: {
						color,
						icon,
						conditions,
						'translations.$.name': name
					}
				}
			);
		}
	},
	Achievement: {
		translation: (parent, { locale }) => {
			return parent.translations.find((doc) => doc.locale === locale);
		}
	},
	AchievementCondition: {
		park: (parent, args, { Park }) => {
			return Park.findById(parent.parkId);
		},
		ride: (parent, args, { Ride }) => {
			return Ride.findById(parent.rideId);
		}
	}
};
