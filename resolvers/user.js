import jwt from 'jsonwebtoken';
import { combineResolvers } from 'graphql-resolvers';
import { isAdmin } from './authorization';
import { AuthenticationError, UserInputError } from 'apollo-server';

const createToken = (user, secret, expiresIn) => {
	const { _id, email, username, role } = user;
	return jwt.sign({ _id, email, username, role }, secret, {
		expiresIn
	});
};

export default {
	Query: {
		users: (parent, args, { User }) => {
			return User.find({});
		},
		user: (parent, { id, username }, { User }) => {
			if (id) {
				return User.findById(id);
			}

			if (username) {
				return User.findOne({ username });
			}
		},
		me: (parent, args, { User, me }) => {
			if (!me) {
				return null;
			}
			return User.findById(me._id);
		}
	},
	Mutation: {
		createUser: async (parent, { username, email, password }, { User, secret }) => {
			const user = new User({
				username,
				email,
				password,
				role: 'user'
			});

			await user.save();
			return { token: createToken(user, secret, '365d') };
		},
		signIn: async (parent, { login, password }, { User, secret }) => {
			const user = await User.findByLogin(login);

			if (!user) {
				throw new UserInputError('No user found with this login credentials.');
			}

			const isValid = await user.validatePassword(password);

			if (!isValid) {
				throw new AuthenticationError('Invalid password.');
			}
			return { token: createToken(user, secret, '365d') };
		},
		deleteUser: combineResolvers(isAdmin, async (parent, { id }, { User }) => {
			return await User.findOneAndDelete({ _id: id }, { maxTimeMS: 5 })
				.then((resp) => {
					if (resp) {
						console.log('Deleted a document', resp);
						return true;
					}
					return false;
				})
				.catch((err) => {
					console.error(err);
					return false;
				});
		}),
		addParkToCount: async (parent, { parkId }, { me, User, Park }) => {
			if (!me) {
				return null;
			}
			const currentUser = await User.findById(me._id);
			const targetPark = await Park.findById(parkId);

			if (targetPark === null) {
				return null;
			}
			const user = new User({
				parkVisits: [
					...currentUser.parkVisits,
					{
						parkId
					}
				],
				rideVisits: [ ...currentUser.rideVisits ]
			});
			const upsertData = user.toObject();
			delete upsertData._id;
			return User.findOneAndUpdate({ _id: currentUser._id }, { $set: upsertData }, { new: true });
		},
		addRideToCount: async (parent, { rideId }, { me, User, Ride, Achievement }) => {
			// If there is no user, stop the resolver.
			if (!me) {
				return null;
			}
			// Find the ride that the user wants to increment his count for
			const targetRide = await Ride.findById(rideId);
			// If no ride is found, stop the resolver.
			if (targetRide === null) {
				return null;
			}

			// Get the current User and get the achievementProgress off of it.
			const currentUser = await User.findById(me._id);
			const { achievementProgress } = currentUser;
			// Find all achievements which have the current rideId
			const relevantAchievements = await Achievement.find({ 'conditions.rideId': rideId });
			console.log(relevantAchievements);
			// Map the conditionId of the relevant achievements to the array itself, for later use.
			const relevantAchievementsWithRelevantConditions = relevantAchievements.map((obj) => {
				return {
					id: obj.id,
					conditionId: obj.conditions.find((cond) => cond.rideId === rideId).id,
					amountOfConditions: obj.conditions.length
				};
			});
			// Create an empty array for later use.
			let achievementProgressArray = [];

			// If the user has no achievement progress at all
			if (achievementProgress.length === 0 && relevantAchievements.length > 0) {
				// fill the achievement progress array with relevant entries on the found achievements
				achievementProgressArray = relevantAchievementsWithRelevantConditions.map((obj) => {
					return {
						achievementId: obj.id,
						completedConditions: [ obj.conditionId ],
						isCompleted: obj.amountOfConditions === 1
					};
				});
			} else if (achievementProgress.length === 0 && relevantAchievements.length > 0) {
				achievementProgressArray = [];
			} else {
				// Returns achievements which the user already has progress in and is in the userProgress array.
				const presentAchievements = relevantAchievementsWithRelevantConditions.filter((obj) =>
					achievementProgress.some((item) => item.achievementId === obj.id)
				);
				console.log(presentAchievements);
				// Returns achievements which the user does not have progress in and is not in the userProgress array.
				const notFoundAchievements = relevantAchievementsWithRelevantConditions.filter((obj) =>
					achievementProgress.some((item) => item.achievementId !== obj.id)
				);
				console.log(notFoundAchievements);
				// If there are both present achievements in the achievementprogress array and not found achievements
				if (presentAchievements.length > 0 && notFoundAchievements > 0) {
					console.log('both');
					// Maps the not found achievements to an array.
					const notFoundAchievementsArray = notFoundAchievements.map((obj) => {
						return {
							achievementId: obj.id,
							completedConditions: [ obj.conditionId ],
							isCompleted:
								achievementProgress.find((ach) => ach.achievementId === obj.id).completedConditions
									.length +
									1 ===
								obj.amountOfConditions
						};
					});

					// Returns the achievements with their check that the user has progress in with the current checks.
					// Essentially achievement progress where the user has fulfilled the check
					const presentWithConditions = presentAchievements.filter((obj) =>
						achievementProgress.some((item) =>
							item.completedConditions.some((cond) => cond === obj.conditionId)
						)
					);

					// Returns the achievements with their check that the user has no progress in with the current checks.
					// Essentially achievement progress where the user has not fulfilled the check OR where there are other checks.
					const presentWithoutConditions = presentAchievements.filter((obj) =>
						achievementProgress.some((item) =>
							item.completedConditions.some((cond) => cond !== obj.conditionId)
						)
					);

					// Filters the achievements to the entries from presentWithoutConditions with the presentWithConditions
					// To fix the duplicates of presentWithoutConditions
					const presentWithRelevantEmptyConditions = presentWithoutConditions.filter((obj) =>
						presentWithConditions.some((item) => item.id !== obj.id)
					);

					// maps the relevant empty checks, where the achievemets are present to the achievementProgressArray
					const relevantAchievementsArray = presentWithRelevantEmptyConditions.map((obj) => {
						return {
							achievementId: obj.id,
							completedConditions: [
								obj.conditionId,
								...achievementProgress.find((ach) => ach.id === obj.id).completedConditions
							],
							isCompleted:
								achievementProgress.find((ach) => ach.achievementId === obj.id).completedConditions
									.length +
									1 ===
								obj.amountOfConditions
						};
					});

					// Put both the not found and relevant achievements in the array.
					achievementProgressArray = [ ...notFoundAchievementsArray, ...relevantAchievementsArray ];

					// If there are only present achievement
				} else if (presentAchievements.length > 0) {
					console.log('present');
					// Returns the achievements with their check that the user has progress in with the current checks.
					// Essentially achievement progress where the user has fulfilled the check
					const presentWithoutConditions = presentAchievements.filter((obj) =>
						achievementProgress.some((item) =>
							item.completedConditions.some((cond) => cond !== obj.conditionId)
						)
					);

					// Returns the achievements with their check that the user has no progress in with the current checks.
					// Essentially achievement progress where the user has not fulfilled the check OR where there are other checks.
					const presentWithConditions = presentAchievements.filter((obj) =>
						achievementProgress.some((item) =>
							item.completedConditions.some((cond) => cond === obj.conditionId)
						)
					);

					// If there are achievements in the achievementProgress with their checks, to filter them with the not founds
					if (presentWithConditions.length > 0) {
						// Filters the achievements to the entries from presentWithoutConditions with the presentWithConditions
						// To fix the duplicates of presentWithoutConditions
						const presentWithRelevantEmptyChecks = presentWithoutConditions.filter((obj) =>
							presentWithConditions.some((item) => item.id !== obj.id)
						);

						// Maps the relevant empty check achievements to the achievement progress array
						achievementProgressArray = presentWithRelevantEmptyChecks.map((obj) => {
							return {
								achievementId: obj.id,
								completedConditions: [
									obj.conditionId,
									...achievementProgress.find((ach) => ach.achievementId === obj.id)
										.completedConditions
								],
								isCompleted:
									achievementProgress.find((ach) => ach.achievementId === obj.id).completedConditions
										.length +
										1 ===
									obj.amountOfConditions
							};
						});

						// If there are only achievements where the checks are not found, but also not present checks
					} else if (presentWithoutConditions.length > 0 && presentWithConditions.length === 0) {
						console.log('something else');
						// maps the relevant empty checks, where the achievemets are present to the achievementProgressArray
						achievementProgressArray = presentWithoutConditions.map((obj) => {
							return {
								achievementId: obj.id,
								completedConditions: [
									obj.conditionId,
									...achievementProgress.find((ach) => ach.achievementId === obj.id)
										.completedConditions
								],
								isCompleted:
									achievementProgress.find((ach) => ach.achievementId === obj.id).completedConditions
										.length +
										1 ===
									obj.amountOfConditions
							};
						});
					}

					// If there are only not found achievements.
				} else if (notFoundAchievements.length > 0) {
					// map all not not found achievements.
					achievementProgressArray = notFoundAchievements.map((obj) => {
						return {
							achievementId: obj.id,
							completedConditions: [ obj.conditionId ],
							isCompleted: obj.amountOfConditions === 1
						};
					});
				}
			}
			let filteredAchievementProgress = [];
			if (currentUser.achievementProgress.length > 0 && achievementProgressArray.length > 0) {
				filteredAchievementProgress = currentUser.achievementProgress.filter((obj) =>
					achievementProgressArray.some((item) => item.achievementId !== obj.achievementId)
				);
				const user = new User({
					parkVisits: [ ...currentUser.parkVisits ],
					rideVisits: [
						...currentUser.rideVisits,
						{
							rideId
						}
					],
					achievementProgress: [ ...filteredAchievementProgress, ...achievementProgressArray ]
				});
				const upsertData = user.toObject();
				delete upsertData._id;
				return User.findOneAndUpdate({ _id: currentUser._id }, { $set: upsertData }, { new: true });

				// If no achievement docs needs updating
			} else if (currentUser.achievementProgress.length > 0 && achievementProgressArray.length === 0) {
				const user = new User({
					parkVisits: [ ...currentUser.parkVisits ],
					rideVisits: [
						...currentUser.rideVisits,
						{
							rideId
						}
					],
					achievementProgress: currentUser.achievementProgress
				});
				const upsertData = user.toObject();
				delete upsertData._id;
				return User.findOneAndUpdate({ _id: currentUser._id }, { $set: upsertData }, { new: true });
			} else if (currentUser.achievementProgress.length === 0 && achievementProgressArray.length > 0) {
				const user = new User({
					parkVisits: [ ...currentUser.parkVisits ],
					rideVisits: [
						...currentUser.rideVisits,
						{
							rideId
						}
					],
					achievementProgress: achievementProgressArray
				});
				const upsertData = user.toObject();
				delete upsertData._id;
				return User.findOneAndUpdate({ _id: currentUser._id }, { $set: upsertData }, { new: true });
			} else if (currentUser.achievementProgress.length === 0 && achievementProgressArray.length === 0) {
				const user = new User({
					parkVisits: [ ...currentUser.parkVisits ],
					rideVisits: [
						...currentUser.rideVisits,
						{
							rideId
						}
					],
					achievementProgress: []
				});
				const upsertData = user.toObject();
				delete upsertData._id;
				return User.findOneAndUpdate({ _id: currentUser._id }, { $set: upsertData }, { new: true });
			}
		}
	},
	User: {
		friendships: (parent, args, { Friendship }) => {
			return Friendship.find({ status: 'accepted' }).or([
				{ requesterId: parent._id },
				{ addresseeId: parent._id }
			]);
		},
		friendshipRequests: (parent, args, { Friendship }) => {
			return Friendship.find({ status: 'pending' }).and([ { addresseeId: parent._id } ]);
		}
	},
	UserParkVisit: {
		park: (parent, args, { Park }) => {
			return Park.findById(parent.parkId);
		},
		user: (parent, args, { User }) => {
			return parent.parent();
		}
	},
	UserRideVisit: {
		ride: (parent, args, { Ride }) => {
			return Ride.findById(parent.rideId);
		},
		user: (parent, args, { User }) => {
			return parent.parent();
		}
	},
	UserAchievementProgress: {
		achievement: (parent, args, { Achievement }) => {
			return Achievement.findById(parent.achievementId);
		}
	}
};
