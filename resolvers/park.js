import { combineResolvers } from 'graphql-resolvers';
import { isAuthenticated } from './authorization';

export default {
	Query: {
		parks: (parent, { offset = 0, limit = 100, filter = null }, { Park }) => {
			if (filter === null) {
				return Park.find({}).skip(offset).limit(limit);
			} else {
				return Park.find({ originalName: { $regex: filter, $options: 'i' } });
			}
		},
		park: async (parent, { id, slug }, { Park }) => {
			if (id) {
				return Park.findById(id);
			}
			if (slug) {
				const park = await Park.findOne({ slug });
				return park;
			}
		}
	},
	Mutation: {
		createPark: combineResolvers(isAuthenticated, async (parent, { name, tempImage }, { Park }) => {
			const park = new Park({
				originalName: name,
				slug: name.toLowerCase().replace(/ /g, '-'),
				tempImage,
				translations: [
					{
						name
					}
				]
			});
			return park.save();
		}),
		deletePark: combineResolvers(async (parent, { id }, { Park }) => {
			return await Park.findOneAndDelete({ _id: id }, { maxTimeMS: 5 })
				.then((resp) => {
					if (resp) {
						return true;
					} else {
						return false;
					}
				})
				.catch(() => {
					return false;
				});
		}),
		updatePark: async (parent, { id, name, tempImage }, { Park }) => {
			const existingPark = await Park.findById(id);
			if (existingPark === null) {
				return null;
			}

			const existingTranslation = existingPark.translations.find((trans) => trans.locale === 'en');

			return await Park.findOneAndUpdate(
				{ _id: id, 'translations._id': existingTranslation.id },
				{
					$set: {
						originalName: name,
						slug: name.toLowerCase().replace(/ /g, '-'),
						tempImage: tempImage,
						'translations.$.name': name
					}
				}
			);
		}
	},
	Park: {
		translation: (parent, { locale }) => {
			return parent.translations.find((doc) => doc.locale === locale);
		},
		rides: (parent, args, { Ride }) => {
			return Ride.find({ parkId: parent._id });
		}
	}
};
