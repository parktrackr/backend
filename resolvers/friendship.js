export default {
	Query: {
		friendships: (parent, args, { Friendship }) => {
			return Friendship.find({});
		}
	},
	Mutation: {
		createFriendship: async (parent, { userId }, { me, Friendship }) => {
			const friendship = new Friendship({
				requesterId: me._id,
				addresseeId: userId
			});
			return friendship.save();
		},
		acceptFriendship: async (parent, { friendshipId }, { me, Friendship }) => {
			const filter = { _id: friendshipId, addresseeId: me._id };
			const update = { status: 'accepted' };

			return Friendship.findOneAndUpdate(filter, update, {
				new: true
			});
		}
	},
	Friendship: {
		requester: (parent, args, { User }) => {
			return User.findById(parent.requesterId);
		},
		addressee: (parent, args, { User }) => {
			return User.findById(parent.addresseeId);
		}
	}
};
