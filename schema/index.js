import { gql } from 'apollo-server-express';

import userSchema from './user';
import parkSchema from './park';
import rideSchema from './ride';
import friendshipSchema from './friendship';
import achievementSchema from './achievement';

const linkSchema = gql`
	scalar Date

	type Query {
		_: Boolean
	}

	type Mutation {
		_: Boolean
	}

	type Subscription {
		_: Boolean
	}
`;

export default [ linkSchema, userSchema, friendshipSchema, parkSchema, rideSchema, achievementSchema ];
