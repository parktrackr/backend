import { gql } from 'apollo-server-express';

export default gql`
	extend type Query {
		users: [User!]
		user(id: ID, username: String): User
		me: User
	}

	extend type Mutation {
		createUser(username: String!, email: String!, password: String!): Token!
		signIn(login: String!, password: String!): Token!
		deleteUser(id: ID!): Boolean!
		addParkToCount(parkId: ID!): User
		addRideToCount(rideId: ID!): User
	}

	type Token {
		token: String!
	}

	type User {
		id: ID!
		username: String!
		email: String!
		role: String!
		parkVisits: [UserParkVisit!]
		rideVisits: [UserRideVisit!]
		achievementProgress: [UserAchievementProgress!]
		friendships: [Friendship!]
		friendshipRequests: [Friendship!]
		registerDate: Date!
	}

	type UserParkVisit {
		id: ID!
		user: User!
		park: Park!
		visitDate: Date!
	}

	type UserRideVisit {
		id: ID!
		user: User!
		ride: Ride!
		visitDate: Date!
	}

	type UserAchievementProgress {
		id: ID!
		achievement: Achievement!
		completedConditions: [String!]!
		isCompleted: Boolean!
		completionDate: Date
	}
`;
