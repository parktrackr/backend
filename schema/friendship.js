import { gql } from 'apollo-server-express';

export default gql`
	extend type Query {
		friendships: [Friendship!]
	}

	extend type Mutation {
		createFriendship(userId: String!): Friendship
		acceptFriendship(friendshipId: String): Friendship
	}

	type Friendship {
		id: ID!
		requester: User!
		addressee: User!
		status: String!
		createdAt: Date!
	}
`;
