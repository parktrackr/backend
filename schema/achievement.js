import { gql } from 'apollo-server-express';

export default gql`
	extend type Query {
		achievements: [Achievement!]
		achievement(id: ID): Achievement
	}

	extend type Mutation {
		createAchievement(name: String!, color: String!, icon: String!, rideIds: [String!]): Achievement!
		deleteAchievement(id: ID!): Boolean!
		updateAchievement(id: ID!, name: String!, color: String!, icon: String!, rideIds: [String!]): Achievement!
	}

	type Achievement {
		id: ID!
		color: String!
		icon: String!
		conditions: [AchievementCondition!]
		translation(locale: String!): AchievementTranslation
		createdAt: Date!
	}

	type AchievementTranslation {
		id: ID!
		locale: String!
		name: String!
		createdAt: Date!
	}

	type AchievementCondition {
		id: ID!
		park: Park
		ride: Ride
		createdAt: Date!
		lastModified: Date!
	}
`;
