import { gql } from 'apollo-server-express';

export default gql`
	extend type Query {
		parks(offset: Int, limit: Int, filter: String): [Park!]
		park(id: ID, slug: String): Park
	}

	extend type Mutation {
		createPark(name: String!, tempImage: String!): Park!
		deletePark(id: ID!): Boolean!
		updatePark(id: ID!, name: String!, tempImage: String!): Park!
	}

	type Park {
		id: ID!
		originalName: String!
		slug: String!
		translation(locale: String!): ParkTranslation
		tempImage: String!
		createdAt: Date!
		rides: [Ride!]
	}

	type ParkTranslation {
		id: ID!
		locale: String!
		name: String!
		createdAt: Date!
	}
`;
