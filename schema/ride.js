import { gql } from 'apollo-server-express';

export default gql`
	extend type Query {
		rides(offset: Int, limit: Int, filter: String): [Ride!]
		ride(id: ID, slug: String): Ride
		rideCategories(offset: Int, limit: Int): [RideCategory!]
		rideCategory(id: ID, slug: String): RideCategory
		rideTypes(offset: Int, limit: Int): [RideType!]
		rideType(id: ID, slug: String): RideType
	}

	extend type Mutation {
		createRide(
			parkId: String!
			name: String!
			rideCategoryIds: [String!]!
			rideTypeIds: [String!]!
			intensity: Int!
			height: Int
			length: Int
			tempImage: String!
		): Ride!
		deleteRide(id: ID!): Boolean!
		updateRide(
			id: ID!
			parkId: String!
			name: String!
			rideCategoryIds: [String!]!
			rideTypeIds: [String!]!
			intensity: Int!
			height: Int
			length: Int
			tempImage: String!
		): Ride!
		createRideCategory(name: String!, color: String!, icon: String!): RideCategory!
		deleteRideCategory(id: ID!): Boolean!
		updateRideCategory(id: ID!, name: String!, color: String!, icon: String!): RideCategory!
		createRideType(name: String!, rideCategoryIds: [String!]!): RideCategory!
		deleteRideType(id: ID!): Boolean!
	}

	type Ride {
		id: ID!
		originalName: String!
		slug: String!
		translation(locale: String!): RideTranslation
		park: Park!
		rideCategories: [RideCategory!]
		rideTypes: [RideType!]
		intensity: Int
		height: Int
		length: Int
		createdAt: Date!
		tempImage: String
	}

	type RideTranslation {
		id: ID!
		locale: String!
		name: String!
		createdAt: Date!
	}

	type RideCategory {
		id: ID!
		originalName: String!
		slug: String!
		color: String!
		icon: String!
		rideTypes: [RideType!]
		translation(locale: String!): RideCategoryTranslation
		createdAt: Date!
		lastModified: Date!
	}

	type RideCategoryTranslation {
		id: ID!
		locale: String!
		name: String!
		createdAt: Date!
	}

	type RideType {
		id: ID!
		originalName: String!
		slug: String!
		rideCategories: [RideCategory!]
		translation(locale: String!): RideTypeTranslation
		createdAt: Date!
		lastModified: Date!
	}

	type RideTypeTranslation {
		id: ID!
		locale: String!
		name: String!
		createdAt: Date!
	}
`;
