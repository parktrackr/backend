import express from 'express';
import jwt from 'jsonwebtoken';
import { ApolloServer, AuthenticationError } from 'apollo-server-express';
import mongoose from 'mongoose';
import depthLimit from 'graphql-depth-limit';
import http from 'http';
require('dotenv').config();

import schema from './schema';
import resolvers from './resolvers';
import models from './models';

const app = express();

mongoose.connect(
	'mongodb://admin:' +
		process.env.ADMIN_KEY +
		'@' +
		process.env.MONGODB_HOST +
		':' +
		process.env.MONGODB_POST +
		'/parktrackr',
	{ useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true }
);
mongoose.connection.once('open', () => {
	console.log(`Connected to: MongoDB ${process.env.MONGODB_HOST}:27017/parktrackr`);
});

const getMe = (token) => {
	if (token.includes('Bearer ')) {
		const cleanToken = token.slice(7, token.length).trimLeft();
		try {
			return jwt.verify(cleanToken, process.env.JWT_SECRET);
		} catch (e) {
			throw new AuthenticationError('Your session expired. Sign in again.');
		}
	}
};

const server = new ApolloServer({
	typeDefs: schema,
	resolvers,
	introspection: true,
	playground: true,
	context: async ({ req, connection }) => {
		if (connection) {
			return {
				...models
			};
		}
		if (req) {
			const token = req.headers.authorization || req.headers['x-token'] || '';
			const me = getMe(token);
			return {
				...models,
				me,
				secret: process.env.JWT_SECRET
			};
		}
	},
	validationRules: [ depthLimit(5) ]
});

server.applyMiddleware({ app, path: '/graphql' });

const httpServer = http.createServer(app);
server.installSubscriptionHandlers(httpServer);

httpServer.listen({ port: process.env.LOCAL_PORT }, () => {
	console.log('Apollo Server on http://localhost:' + process.env.LOCAL_PORT + '/graphql');
});
