This project is being developed for [Project Rode](http://projectrode.nl).

## Instructions for first development

Always grab the develop branch, master branch is purely for deployment. Make features, hotfixes, bugfixes in their appropriate branches. From develop branch, make releases, and those merge into the master branch.

## Instructions for Git Flow

Initialize Git Flow

### `git flow init`

Create a feature branch

### `git flow feature start YOUR_FEATURE`

Finish feature branch

### `git flow feature finish YOUR_FEATURE`

Create a release branch

### `git flow release start 0.1.0`

Finish release branch

### `git flow release finish '0.1.0'`

